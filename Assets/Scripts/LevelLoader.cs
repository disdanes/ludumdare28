﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

	public int level = 1;

	private bool display = false;

	// Use this for initialization
	void Start () {
		for(int i = 0; i< ApplicationModel.availableLevels.Length; i++){
			if(ApplicationModel.availableLevels[i] == level){
				display = true;
			}
		}
		if(!display){
			Destroy (gameObject);
		}

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnMouseOver() {
		if(Input.GetMouseButtonDown(0)){
			Debug.Log(level);
			ApplicationModel.currentLevel = level;
			Application.LoadLevel("level" + level);
		}
	}
}
