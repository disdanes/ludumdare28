﻿using UnityEngine;
using System.Collections;

public class LevelEnd : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D (Collision2D col){
		if(col.gameObject.tag == "Player"){
			Debug.Log(ApplicationModel.currentLevel);
			if(ApplicationModel.currentLevel + 1 == 7){
				Application.LoadLevel("win");
			}else{
				ApplicationModel.availableLevels[ApplicationModel.currentLevel] = ApplicationModel.currentLevel + 1;
				Application.LoadLevel("level_select");
			}
		}
	}
}
