﻿using UnityEngine;
using System.Collections;

public class PlayerEvolve : MonoBehaviour {
	private bool paused = false;
	private GameObject screenOverlay;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(paused){
			//Time.timeScale = 0;
			
		}else {
			//Time.timeScale = 1;

		}
	}

	void OnMouseOver(){
		if(Input.GetMouseButtonDown(0)){
			paused = !paused;
			if(!ApplicationModel.evolved){
				if(!paused){
					foreach (Transform childTransform in screenOverlay.transform){
						DestroyImmediate(childTransform.gameObject);
					}
					DestroyImmediate(screenOverlay);
				}else {
					Object evolveScreen = Resources.Load("EvolveScreen");
					screenOverlay = (GameObject)Instantiate(evolveScreen, ApplicationModel.player.transform.position, Quaternion.identity);
				}
			}
		}
	}
}
