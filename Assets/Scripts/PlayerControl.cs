﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;			
	
	public float moveForce = 365f;			
	public float maxSpeed = 5f;			
	public float jumpForce = 1000f;			
	
	private Transform groundDetector;		
	private bool grounded = false;
	
	void Awake()
	{
		// Setting up references.
		groundDetector = transform.Find("GroundDetector");
	}
	
	void Update()
	{
		if(Input.GetMouseButtonDown(0)){
			if(ApplicationModel.evolution == "star"){
				transform.Rotate(new Vector3(0, 0, 240));
			}else if(ApplicationModel.evolution == "narwhal"){
				if(!facingRight){
					rigidbody2D.AddForce(new Vector2( 1000f, 100f));
				}else {
					rigidbody2D.AddForce(new Vector2( -1000f, 100f));
				}
			}else if(ApplicationModel.evolution == "seahorse"){
				maxSpeed = 8f;
				moveForce = 500f;
				transform.Rotate(new Vector3(0, 0, 120));
			}else if(ApplicationModel.evolution == "octo"){
				transform.Rotate(new Vector3(0, 0, 240));
				if(!facingRight){
					rigidbody2D.AddForce(new Vector2( 2000f, -100f));
				}else {
					rigidbody2D.AddForce(new Vector2( -2000f, -100f));
				}
			}else if(ApplicationModel.evolution == "shark"){
				if(!facingRight){
					rigidbody2D.AddForce(new Vector2( 8000f, -100f));
				}else {
					rigidbody2D.AddForce(new Vector2( -8000f, -100f));
				}
			}else if(ApplicationModel.evolution == "turtle"){
				if(!facingRight){
					rigidbody2D.AddForce(new Vector2( 4000f, -100f));
				}else {
					rigidbody2D.AddForce(new Vector2( -4000f, -100f));
				}
			}







		}
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		grounded = Physics2D.Linecast(transform.position, groundDetector.position, 1 << LayerMask.NameToLayer("Ground"));  
		
		// If the jump button is pressed and the player is grounded then the player should jump.
		if(Input.GetButtonDown("Jump") && grounded)
			jump = true;
	}
	
	void FixedUpdate ()
	{
		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal");
		
		if(h * rigidbody2D.velocity.x < maxSpeed) 			
			rigidbody2D.AddForce(Vector2.right * h * moveForce); 		 		
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);
		
		// If the player should jump...
		if(jump)
		{	
			rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			jump = false;
		}

		if(h > 0 && facingRight){
			Flip();
		} else if (h < 0 && !facingRight){
			Flip();
		}
	}

	void Flip(){
		facingRight = !facingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
