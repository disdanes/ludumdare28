﻿using UnityEngine;
using System.Collections;

public class LevelStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(ApplicationModel.evolution == "star"){
			Object player = Resources.Load("Player");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "jelly"){
			Object player = Resources.Load("PlayerJelly");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "narwhal"){
			Object player = Resources.Load("PlayerNarwhal");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "turtle"){
			Object player = Resources.Load("PlayerTurtle");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "octo"){
			Object player = Resources.Load("PlayerOcto");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "boss"){
			Object player = Resources.Load("PlayerBoss");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "seahorse"){
			Object player = Resources.Load("PlayerSeahorse");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}else if (ApplicationModel.evolution == "shark"){
			Object player = Resources.Load("PlayerShark");
			GameObject p = (GameObject)Instantiate(player, new Vector3(0,0,1), Quaternion.identity);
			ApplicationModel.player = p;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
