using UnityEngine;
using System.Collections;

public class ApplicationModel : MonoBehaviour {

	static public int currentLevel = 1;
	static public int playerLevel = 1;
	static public int playerPoints = 0;
	static public int playerHealth = 120;
	static public int[] availableLevels = new int[6]{1,0,0,0,0,0};
	static public string evolution = "star";
	static public GameObject player;
	static public bool evolved = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
