﻿using UnityEngine;
using System.Collections;

public class PlayerHP : MonoBehaviour {

	private PlayerHealth playerHealth;	// Reference to the player health script.
	private PlayerControl playerControl;	// Reference to the player control script.
	private float maxHP;			// The score in the previous frame.
	
	void Start() {
		// Setting up the reference.
		playerControl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
		maxHP = ApplicationModel.playerHealth;

	}
	void Awake (){

	}
	
	
	void Update (){
		// Set the health text
		if(playerHealth.health <= 0){
			guiText.text = "HP: Dead";
		} else {
			guiText.text = "HP: " + playerHealth.health + "/" + maxHP;
		}
		//previousScore = score;
	}
}
