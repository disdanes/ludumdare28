using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	public float moveSpeed = 0.1f;		// The speed the enemy moves at.
	public int HP = 2;					// How many times the enemy can be hit before it dies.
	public Sprite deadEnemy;			// A sprite of the enemy when it's dead.
	public Sprite damagedEnemy;			// An optional sprite of the enemy when it's damaged.
	public AudioClip[] deathClips;		// An array of audioclips that can play when the enemy dies.
	public GameObject experiencePointsUI;	// A prefab of 100 that appears when the enemy dies.
	public float deathSpinMin = -100f;			// A value to give the minimum amount of Torque when dying
	public float deathSpinMax = 100f;			// A value to give the maximum amount of Torque when dying
	public float aggroDistance = 10f;  // A value representing the aggro distance of the enemy
	public float attackDamage = 10f;	//Amount of damage the enemy does
	public float attackKnockback = 10f; //Amount of distance the player is knocked back
	public int experiencePoints = 10;	//Amount of experience the enemy gives the player


	private SpriteRenderer ren;			// Reference to the sprite renderer.
	private Transform frontCheck;		// Reference to the position of the gameobject used for checking if something is in front.
	private bool dead = false;			// Whether or not the enemy is dead.
	private Vector2 lastForce;			// The last force vector applied to the enemy
	private bool flip = false;   		// Do we need to flip the direction of the enemy
	private bool isFlipped = false;		// Is the enemy currently flipped
	private Vector2 startingPosition;  	// The starting position we want the enemy to return to if they are outside the player aggro range

	private bool playerInRange;

	void Start() {
		//startingPosition = new Vector2(transform.position.x, transform.position.y);
		//Debug.Log (" X: " + startingPosition.x);
		Physics2D.IgnoreLayerCollision(1 << LayerMask.NameToLayer("Enemy"),1 << LayerMask.NameToLayer("Enemy"));
	}

	void Awake()
	{
		// Setting up the references.
		//ren = transform.Find("Player").GetComponent<SpriteRenderer>();
 		//frontCheck = transform.Find("frontCheck").transform;
		//score = GameObject.Find("Score").GetComponent<Score>();
		//Lets send a raycast down from the enemy to see if they hit the player

	}

	void FixedUpdate ()
	{
		// Create an array of all the colliders in front of the enemy.
		/*Collider2D[] frontHits = Physics2D.OverlapPointAll(frontCheck.position, 1);

		// Check each of the colliders.
		foreach(Collider2D c in frontHits)
		{
			// If any of the colliders is an Obstacle...
			if(c.tag == "Obstacle")
			{
				// ... Flip the enemy and stop checking the other colliders.
				Flip ();
				break;
			}
		}
		*/
		if(isFlipped && !flip){
			Flip ();
			isFlipped = false;
		}
		if(flip && !isFlipped){
			Flip();
		}

		Collider2D col = Physics2D.OverlapCircle(transform.position, aggroDistance, 1 << LayerMask.NameToLayer("Player"));
		//foreach (Collider2D col in collider) {
		if(col != null){
			if(col.gameObject.tag == "Player" && col.gameObject != null){
				//col.isTrigger = true;
				float distanceY = col.gameObject.transform.position.y - transform.position.y;
				float distanceX = col.gameObject.transform.position.x - transform.position.x;
				float directionX = distanceX / Mathf.Abs(distanceX);
				float directionY = distanceY / Mathf.Abs(distanceY);
				//Debug.Log("hit: " + distanceX + "," + distanceY);
				if(distanceX >= 0f){
					flip = true;
				} else {
					flip = false;
				}
				if( (Mathf.Abs(distanceX) < aggroDistance * 0.6) && 
				   (Mathf.Abs(distanceX) > aggroDistance * 0.3) &&
				   (Mathf.Abs(distanceY) < aggroDistance * 0.6) &&
				   (Mathf.Abs(distanceY) > aggroDistance * 0.3) ){
					rigidbody2D.AddForceAtPosition(new Vector2( directionX * moveSpeed * 4f, directionY * moveSpeed * 4f), transform.position);
				}else{
					// Set the enemy's velocity to moveSpeed in the x direction.
					lastForce = new Vector2( directionX * moveSpeed * 0.3f, directionY * moveSpeed * 0.3f);
					rigidbody2D.AddForce(lastForce);
				}

			}
		}else {
			float distanceY = Random.Range(-5, 5) - transform.position.y;
			float distanceX = Random.Range(-5, 5) - transform.position.x;
			float directionX = distanceX / Mathf.Abs(distanceX);
			float directionY = distanceY / Mathf.Abs(distanceY);
			lastForce = new Vector2(directionX * 0.02f * moveSpeed, directionY * 0.02f * moveSpeed);
			rigidbody2D.AddForce(lastForce);
		}
		//}
		// If the enemy has one hit point left and has a damagedEnemy sprite...
		//if(HP == 1 && damagedEnemy != null)
			// ... set the sprite renderer's sprite to be the damagedEnemy sprite.
			//ren.sprite = damagedEnemy;
			
		// If the enemy has zero or fewer hit points and isn't dead yet...
		if(HP <= 0 && !dead)
			// ... call the death function.
			Death ();
	}
	
	public void Hurt()
	{
		// Reduce the number of hit points by one.
		rigidbody2D.AddForce(Vector2.Scale(rigidbody2D.velocity ,new Vector2(-40f * moveSpeed, -40f * moveSpeed)));
		HP--;

	}
	
	void Death()
	{
		// Find all of the sprite renderers on this object and it's children.
		SpriteRenderer[] otherRenderers = GetComponentsInChildren<SpriteRenderer>();

		// Disable all of them sprite renderers.
		foreach(SpriteRenderer s in otherRenderers)
		{
			s.enabled = false;
		}

		// Re-enable the main sprite renderer and set it's sprite to the deadEnemy sprite.
		//ren.enabled = true;
		//ren.sprite = deadEnemy;

		// Increase the score by 100 points
		//score.score += 100;

		// Set dead to true.
		dead = true;

		// Allow the enemy to rotate and spin it by adding a torque.
		rigidbody2D.fixedAngle = false;
		rigidbody2D.AddTorque(Random.Range(deathSpinMin,deathSpinMax));

		// Find all of the colliders on the gameobject and set them all to be triggers.
		Collider2D[] cols = GetComponents<Collider2D>();
		foreach(Collider2D c in cols)
		{
			c.isTrigger = true;
		}



		// Play a random audioclip from the deathClips array.
		//int i = Random.Range(0, deathClips.Length);
		//AudioSource.PlayClipAtPoint(deathClips[i], transform.position);

		// Create a vector that is just above the enemy.
		Vector3 scorePos;
		scorePos = transform.position;
		scorePos.y += 1.5f;

		ApplicationModel.playerPoints += experiencePoints;

		//Destroy yourself!
		Destroy(gameObject);

		// Instantiate the 100 points prefab at this point.
		//Instantiate(experiencePointsUI, scorePos, Quaternion.identity);

	}


	public void Flip()
	{
		// Multiply the x component of localScale by -1.
		Vector3 enemyScale = transform.localScale;
		enemyScale.x *= -1;
		transform.localScale = enemyScale;
		isFlipped = true;
	}
}
