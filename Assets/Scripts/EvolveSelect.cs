﻿using UnityEngine;
using System.Collections;

public class EvolveSelect : MonoBehaviour {

	public int evolveCost = 100;
	public string evolveName = "star";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseOver() {
		if(Input.GetMouseButtonDown(0)){
			Debug.Log(evolveName);
			if(ApplicationModel.playerPoints >= evolveCost) {
				ApplicationModel.playerPoints -= evolveCost;
				ApplicationModel.evolution = evolveName;
				if(evolveName == "narwhal"){
					ApplicationModel.playerHealth = 140;
				}else if (evolveName == "seahorse"){
					ApplicationModel.playerHealth = 180;
				}else if (evolveName == "octo"){
					ApplicationModel.playerHealth = 200;
				}else if (evolveName == "turtle"){
					ApplicationModel.playerHealth = 220;
				}else if (evolveName == "shark"){
					ApplicationModel.playerHealth = 250;
				}
				ApplicationModel.evolved = true;
				Application.LoadLevel("level" + ApplicationModel.currentLevel);
			}

		}
	}
}
